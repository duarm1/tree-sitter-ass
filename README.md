tree-sitter-ass
==================

This grammar implements the ASS/SSA format.

Overview
--------

Example INI file:

    [section name]
    ; comment
    some_key: some_value
    another-key: another value

    [another section]
    # a comment
    some_key: some_value
    another-key: another value

Known issues
------------

Reference
---------

Release
-------

Steps to perform a release:

1. Bump and tag the version (choose `patch`/`minor`/`major` as appropriate).
   ```
   npm version patch -m "release %s"
   ```
2. Bump to prerelease, without creating a tag .
   ```
   npm version --no-git-tag-version prerelease --preid dev && git add package*.json && git commit -m bump
   ```
3. Push.
   ```
   git push --follow-tags
   ```
4. Release the tagged commit

